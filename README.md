L2py-server-docker
==================

WARNING
-------

By default inner API is exposed. Please close it when deploying.

How to use
----------

1. Use `build.sh` for building docker images.
2. Use `docker-compose up` for starting docker containers.
