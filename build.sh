rm -rf l2py-server-data
rm -rf l2py-server-game
rm -rf l2py-server-login
git clone https://l2py@bitbucket.org/l2py/l2py-server-data.git
git clone https://l2py@bitbucket.org/l2py/l2py-server-game.git
git clone https://l2py@bitbucket.org/l2py/l2py-server-login.git
docker build -t l2py_data l2py-server-data
docker build -t l2py_game l2py-server-game
docker build -t l2py_login l2py-server-login
